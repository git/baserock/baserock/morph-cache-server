#!/usr/bin/python
# Copyright (C) 2012  Codethink Limited
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along
# with this program.  If not, see <http://www.gnu.org/licenses/>.


from distutils.core import setup


setup(name='morph-cache-server',
      description='FIXME',
      long_description='''\
FIXME
''',
      classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Console',
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'Intended Audience :: System Administrators',
        'License :: OSI Approved :: GNU General Public License (GPL)',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python',
        'Topic :: Software Development :: Build Tools',
        'Topic :: Software Development :: Embedded Systems',
        'Topic :: System :: Archiving :: Packaging',
        'Topic :: System :: Software Distribution',
      ],
      author='Jannis Pohlmann',
      author_email='jannis.pohlmann@codethink.co.uk',
      url='http://www.baserock.org/',
      scripts=['morph-cache-server'],
      packages=['morphcacheserver'],
     )
